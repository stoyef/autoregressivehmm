# Autoregressive HMMs

This package enables fitting HMMs with within-state autoregression in the state-dependent process as well as simulating data from said models.

### Functionalities
* computation of partially penalised autoregressive HMM likelihood
* fitting autoregressive HMMs using direct numerical optimisation
* global state decoding
* computation of pseudo residuals
* simulation of data from autoregressive HMMs
* performing full simulation studies of autoregressive HMMs

### Installation

You can install the package in `R` using the following line:

`devtools::install_git("https://gitlab.ub.uni-bielefeld.de/stoyef/autoregressivehmm")`
