### development of coefficients when increasing penalty
lambdas = seq(0,100,length=20)
p_auto=c(5,5,5,5)
dists=c('gamma','vm')
N=2

set.seed(1234)

autos = matrix(NA,ncol= sum(p_auto),nrow=length(lambdas))
aics = rep(NA,length(lambdas))
bics = rep(NA,length(lambdas))
eff_df = c(30,rep(NA, length(lambdas)-1))
llks = rep(NA, length(lambdas))

for (lambda in 1:length(lambdas)){
  cat("starting with lambda = ",lambdas[lambda],'\n',sep='')
  mod = NA
  while (is.na(mod[[1]])){ # need to re-run model fit with different starting pars
    # if model fit failed
    theta=c(-2,-2,
            550,900,200,65, # step
            0,0.05,3,19, # turn
            rep(c(runif(3,0,0.0001),runif(2,0,0.3)),4))
    theta.star = starize(theta, N=2, p=c(5,5,5,5), dists=dists)
    mod = tryCatch({fit_arp_model(mllk=mllk, 
                                  data=tracks_over_1000, 
                                  theta.star=theta.star, 
                                  N=N, 
                                  p_auto=p_auto,
                                  dists=dists,
                                  lambda=lambdas[lambda])},
                   error = function(e){return(NA)})
  }
  
  llks[lambda] = -mod$mllk_optim
  aics[lambda] = mod$AIC
  bics[lambda] = mod$BIC
  if (lambdas[lambda]>0) eff_df[lambda] = mod$effective_df
  auto_count = 0
  for (dist in 1:length(dists)){
    for (state in 1:N){
      autos[lambda,auto_count+1:p_auto[(dist-1)*N+state]] = mod$autocorrelation[[dist]][[state]]
      auto_count = auto_count+p_auto[(dist-1)*N+state]
    }
  }
}

# matrix of estimated autoregressive coefficients (rows are the increasing penalties)
#autos