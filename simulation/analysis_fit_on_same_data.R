# read in results
mllks = read.csv("results/fit_on_same_data/mllks.csv")
aics = read.csv("results/fit_on_same_data/aics.csv")
bics = read.csv("results/fit_on_same_data/bics.csv")
accs = read.csv("results/fit_on_same_data/accs.csv")
dfs = read.csv("results/fit_on_same_data/dfs.csv")
autocor_matrix = read.csv("results/fit_on_same_data/autocor_matrix.csv")
params_matrix = read.csv("results/fit_on_same_data/params_matrix.csv")

## plot results

lambdas = seq(0,100,by=0.25)
#layout(matrix(c(1,2,3,4),nrow=4))
par(mfrow=c(4,1),mai=c(0.6,0.6,0.3,0))
## visualizations
pal=brewer.pal(5,'Dark2')


plot(lambdas[0:401], accs$x[0:401], type='l',bty='n',ylab='accuracy',xlab=expression(lambda),
     main='accuracy of model with minimum BIC for each penalty',
     cex.main=1.5,cex.axis=1,cex.lab=1.5)

plot(lambdas[0:401], aics$x[0:401], type='l',bty='n',ylim=c(16000,17000),
     xlab=expression(lambda),ylab='AIC/BIC',col=pal[1],
     main='AIC/BIC of model with minimum BIC for each penalty',
     cex.main=1.5,cex.axis=1,cex.lab=1.5)
lines(lambdas[0:401], bics$x[0:401],col=pal[2])
legend(90,17100,c('AIC','BIC'),col=pal[1:2],lwd=1,bty='n')
best_lambda_aic = lambdas[which.min(aics$x)]
best_lambda_bic = lambdas[which.min(bics$x)]
cat('Best complexity penalty according to AIC:',best_lambda_aic,'\n')
cat('Best complexity penalty according to BIC:',best_lambda_bic,'\n')
lines(c(best_lambda_aic,best_lambda_aic),c(16000,17000),lty=2)
lines(c(best_lambda_bic,best_lambda_bic),c(16000,17000),lty=2)
text(c(best_lambda_aic-2,best_lambda_bic+2),c(16950,16950),c('AIC','BIC'))


plot(lambdas[0:401], dfs$x[0:401], type='l',bty='n',xlab=expression(lambda),
     ylab='effective df',
     main='effective degrees of freedom of model with minimum BIC for each penalty',
     cex.main=1.5,cex.axis=1,cex.lab=1.5)
#lines(c(0,200),c(30,30),col=2)
#lines(c(0,200),c(10,10),col=2)

plot(lambdas[0:401],-mllks$x[0:401],type='l',bty='n',
     xlab=expression(lambda),ylab='log-Likelihood',
     main='log-likelihood of model with minimum BIC for each penalty',
     cex.main=1.5,cex.axis=1,cex.lab=1.5)

# plot only for AIC/BIC
dev.off()

# This is Figure 1 in supplement
pdf("figures/figS01.pdf", width=12, height=8)
plot(lambdas[1:401], aics$x[1:401], type='l',bty='n',ylim=c(16400,16800),
     xlab=expression(lambda),ylab='AIC/BIC',col=pal[1],
     main='AIC/BIC of model with minimum BIC for each penalty',
     lwd=2)
lines(lambdas[1:401], bics$x[1:401],col=pal[2],lwd=2)
legend('topright',c('AIC','BIC'),col=pal[1:2],lwd=1,bty='n')
best_lambda_aic = lambdas[which.min(aics$x)]
best_lambda_bic = lambdas[which.min(bics$x)]
cat('Best complexity penalty according to AIC:',best_lambda_aic,'\n')
cat('Best complexity penalty according to BIC:',best_lambda_bic,'\n')
lines(c(best_lambda_aic,best_lambda_aic),c(16400,17000),lty=2)
lines(c(best_lambda_bic,best_lambda_bic),c(16400,17000),lty=2)
text(c(best_lambda_aic-2,best_lambda_bic+2),c(16750,16750),c('AIC','BIC'))
graphics.off()

autocor_matrix = autocor_matrix[1:401,]
params_matrix = params_matrix[1:401,]
autocor_matrix$X=lambdas


#### analysis of parameter estimates

# step lengths
dev.off()
plot(lambdas,params_matrix[,2],type='l',bty='n',ylim=c(0,45),col=pal[1],
     xlab=expression(lambda),main='parameter estimates step lengths',
     ylab='parameter estimate')
lines(lambdas,params_matrix[,3],col=pal[2])
lines(lambdas,params_matrix[,4],col=pal[3])
lines(lambdas,params_matrix[,5],col=pal[4])
lines(c(0,100),c(20,20),col=pal[1],lwd=2)
lines(c(0,100),c(40,40),col=pal[2],lwd=2)
lines(c(0,100),c(5,5),col=pal[3],lwd=2)
lines(c(0,100),c(7,7),col=pal[4],lwd=2)
legend(80,33.5,c(expression(mu[1]),expression(mu[2]),expression(sigma[1]),expression(sigma[2])),
       col=pal,lwd=2,bty='n')

# turning angles
plot(lambdas,params_matrix[,6],type='l',bty='n',ylim=c(-5,18),col=pal[1],
     xlab=expression(lambda),main='parameter estimates turning angles',
     ylab='parameter estimate')
lines(lambdas,params_matrix[,7],col=pal[2])
lines(lambdas,params_matrix[,8],col=pal[3])
lines(lambdas,params_matrix[,9],col=pal[4])
lines(c(0,100),c(0,0),col=pal[1],lwd=2)
lines(c(0,100),c(0,0),col=pal[2],lwd=1)
lines(c(0,100),c(2,2),col=pal[3],lwd=2)
lines(c(0,100),c(12,12),col=pal[4],lwd=2)
legend('topright',c(expression(mu[1]),expression(mu[2]),expression(kappa[1]),expression(kappa[2])),
       col=pal,lwd=2,bty='n')

# autoregression
plot(lambdas,autocor_matrix[,2],type='l',bty='n',ylim=c(0,0.75),col=pal[1],
     xlab=expression(lambda),main='parameter estimates autoregression step lengths state 1',
     ylab='parameter estimate')
for (lag in 3:6){
  lines(lambdas,autocor_matrix[,lag],col=pal[lag-1])
}
legend('topright',c('0.3','0.15','0','0','0'),col=pal[5:1],lwd=1.5,
       title='true values',bty='n')
lines(c(0,100),c(0.15,0.15),col=pal[4],lwd=1.5)
lines(c(0,100),c(0.3,0.3),col=pal[5],lwd=1.5)


plot(lambdas,autocor_matrix[,7],type='l',bty='n',ylim=c(0,0.75),col=pal[1],
     xlab=expression(lambda),main='parameter estimates autoregression step lengths state 2',
     ylab='parameter estimate')
for (lag in 2:5){
  lines(lambdas,autocor_matrix[,6+lag],col=pal[lag])
}
legend('topright',c('0.4','0.15','0','0','0'),col=pal[5:1],lwd=1.5,
       title='true values',bty='n')
lines(c(0,100),c(0.15,0.15),col=pal[4],lwd=1.5)
lines(c(0,100),c(0.4,0.4),col=pal[5],lwd=1.5)


plot(lambdas,autocor_matrix[,12],type='l',bty='n',ylim=c(0,0.75),col=pal[1],
     xlab=expression(lambda),main='parameter estimates autoregression turning angles state 1',
     ylab='parameter estimate')
for (lag in 2:5){
  lines(lambdas,autocor_matrix[,11+lag],col=pal[lag])
}
legend('topright',c('0.3','0.2','0','0','0'),col=pal[5:1],lwd=1.5,
       title='true values',bty='n')
lines(c(0,100),c(0.2,0.2),col=pal[4],lwd=1.5)
lines(c(0,100),c(0.3,0.3),col=pal[5],lwd=1.5)


plot(lambdas,autocor_matrix[,17],type='l',bty='n',ylim=c(0,0.75),col=pal[1],
     xlab=expression(lambda),main='parameter estimates autoregression turning angles state 2',
     ylab='parameter estimate')
for (lag in 2:5){
  lines(lambdas,autocor_matrix[,16+lag],col=pal[lag])
}
legend('topright',c('0.4','0.2','0','0','0'),col=pal[5:1],lwd=1.5,
       title='true values',bty='n')
lines(c(0,100),c(0.2,0.2),col=pal[4],lwd=1.5)
lines(c(0,100),c(0.4,0.4),col=pal[5],lwd=1.5)
