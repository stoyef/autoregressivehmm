# 2024-11-15
# Ferdinand Stoye
set.seed(4242)

# Visualize path of AIC and BIC with varying penalty for a fixed dataset.

lambdas = seq(0,100,by=0.25)
n_runs=10
n_samples_simulated = 2000
N = 2
dists=c('gamma','vm')
p_auto=c(5,5,5,5)


simulated_data = sample_arp(n_samples=n_samples_simulated,
                            delta=c(0.5,0.5),
                            Gamma=matrix(c(0.9,0.1,0.1,0.9),nrow=2),
                            N=N,
                            dists=dists,
                            params=c(20,40,5,7,
                                     0,0,2,12),
                            autocor=list(
                              list(c(0.15,0.3),c(0.15,0.4)),
                              list(c(0.2,0.3),c(0.2,0.4))
                            ),
                            p=c(2,2,2,2))

mllks = aics = bics = dfs = accs = rep(NA,length(lambdas))
autocor_matrix = matrix(NA,nrow=length(lambdas)*n_runs,ncol=20,
                        dimnames = list(rep(lambdas,each=n_runs)))
params_matrix = matrix(NA,nrow=length(lambdas)*n_runs,ncol=8,
                       dimnames = list(rep(lambdas,each=n_runs),
                                       c('gamma_mu1','gamma_mu1','gamma_sigma1','gamma_sigma2','vm_mu1','vm_mu2','vm_sigma1','vm_sigma2')))

plot(NA,xlim=c(0,100),ylim=c(16200,16800),xlab=expression(lambda),ylab="AIC/BIC",bty='n')

for (lambda in 1:length(lambdas)){
  start_time = Sys.time()
  cat('...starting with lambda = ', lambdas[lambda], '...  |', sep='')
  
  ################ hard coding of starting parameters, below in for loop
  best_bic = Inf
  mod_best_bic = 0
  for (i in 1:n_runs){
    ############### starting parameters (only for N=2)
    params = c(
      rep(-2,N*(N-1)),
      runif(2,10,50),
      runif(2,3,9),
      runif(2,-0.3,0.3),
      runif(2,1,15),
      runif(3,0,0.1),
      runif(2,0.1,0.3),
      runif(3,0,0.1),
      runif(2,0,0.3),
      runif(3,0,0.1),
      runif(2,0.1,0.3),
      runif(3,0,0.1),
      runif(2,0.1,0.3)
    )
    theta.star = starize(params, N=N, p=p_auto, dists=dists)
    
    assign(paste('mod_',i,sep=''), fit_arp_model(mllk=mllk,
                                                 data=simulated_data$data,
                                                 theta.star=theta.star,
                                                 N=N,
                                                 p_auto = p_auto,
                                                 dists = dists,
                                                 lambda=lambdas[lambda])
    )
    if (length(get(paste('mod_',i,sep='')))<2){ # if model fit didn't work, do this iteration again
      i=i-1
      next
    }
    if (get(paste('mod_',i,sep=''))$BIC < best_bic){ # check if BIC is improved
      mod_best_bic = i
      best_bic = get(paste('mod_',i,sep=''))$BIC
    }
    
    cat('#')
  }
  cat('|\n')
  assign('mod', get(paste('mod_',mod_best_bic, sep='')))
  
  aics[lambda] = mod$AIC
  bics[lambda] = mod$BIC
  mllks[lambda] = mod$mllk_optim
  dfs[lambda] = mod$effective_df
  
  # find rows where state 1 and 2 are swapped and swap back
  swap = which(mod$params[[1]]$mu[1] > 35 & mod$params[[1]]$mu[1] < 45 & mod$params[[1]]$mu[2] > 15 & mod$params[[1]]$mu[2] < 25)
  for (row in swap){
    mod$params[[1]]$mu = rev(mod$params[[1]]$mu)
    mod$params[[1]]$sigma = rev(mod$params[[1]]$sigma)
    mod$params[[2]]$mu = rev(mod$params[[2]]$mu)
    mod$params[[2]]$kappa = rev(mod$params[[2]]$kappa)
    
    hold = mod$autocorrelation[[1]][[1]]
    mod$autocorrelation[[1]][[1]] = mod$autocorrelation[[1]][[2]]
    mod$autocorrelation[[1]][[2]] = hold
    hold = mod$autocorrelation[[2]][[1]]
    mod$autocorrelation[[2]][[1]] = mod$autocorrelation[[2]][[2]]
    mod$autocorrelation[[2]][[2]] = hold
  }
  
  estimated_states = viterbi_arp(x=simulated_data$data,
                                 Gamma = mod$Gamma,
                                 delta = mod$delta,
                                 dists = c('gamma','vm'),
                                 autocor = mod$autocorrelation,
                                 params = mod$params,
                                 N = N,
                                 p = p_auto
  )
  accs[lambda]=sum(simulated_data$states == estimated_states)/n_samples_simulated
  
  for (v in 1:2){
    for (s in 1:2){
      autocor_matrix[lambda,(v-1)*10+(s-1)*5+1:5] = mod$autocorrelation[[v]][[s]]
      params_matrix[lambda,(v-1)*4+(s-1)*2+1:2] = mod$params[[v]][[s]]
    }
  }
  
  points(x=lambdas[lambda],y=mod$AIC, pch=19,cex=0.5, col='cornflowerblue')
  points(x=lambdas[lambda],y=mod$BIC, pch=19,cex=0.5, col='orange')
  cat('...finished with lambda = ', lambdas[lambda], '... (', Sys.time()-start_time,') \n',sep='')
}

# save results
write.csv(mllks, "results/fit_on_same_data/mllks.csv")
write.csv(aics, "results/fit_on_same_data/aics.csv")
write.csv(bics, "results/fit_on_same_data/bics.csv")
write.csv(accs, "results/fit_on_same_data/accs.csv")
write.csv(dfs, "results/fit_on_same_data/dfs.csv")
write.csv(autocor_matrix, "results/fit_on_same_data/autocor_matrix.csv")
write.csv(params_matrix, "results/fit_on_same_data/params_matrix.csv")
