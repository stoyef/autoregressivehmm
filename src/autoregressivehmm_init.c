#include <R.h>
#include <Rinternals.h>
#include <stdlib.h> // for NULL
#include <R_ext/Rdynload.h>

/* FIXME: 
 Check these declarations against the C/Fortran source code.
 */

/* .Call calls */
extern SEXP _autoregressivehmm_dexp_rcpp(SEXP, SEXP, SEXP);
extern SEXP _autoregressivehmm_dgamma_rcpp(SEXP, SEXP, SEXP);
extern SEXP _autoregressivehmm_dlnorm_rcpp(SEXP, SEXP, SEXP);
extern SEXP _autoregressivehmm_dvm_rcpp(SEXP, SEXP, SEXP);
extern SEXP _autoregressivehmm_dweibull_rcpp(SEXP, SEXP, SEXP);
extern SEXP _autoregressivehmm_dwrpcauchy_rcpp(SEXP, SEXP, SEXP);
extern SEXP _autoregressivehmm_forward_cpp(SEXP, SEXP, SEXP, SEXP, SEXP);

static const R_CallMethodDef CallEntries[] = {
  {"_autoregressivehmm_dexp_rcpp",       (DL_FUNC) &_autoregressivehmm_dexp_rcpp,       3},
  {"_autoregressivehmm_dgamma_rcpp",     (DL_FUNC) &_autoregressivehmm_dgamma_rcpp,     3},
  {"_autoregressivehmm_dlnorm_rcpp",     (DL_FUNC) &_autoregressivehmm_dlnorm_rcpp,     3},
  {"_autoregressivehmm_dvm_rcpp",        (DL_FUNC) &_autoregressivehmm_dvm_rcpp,        3},
  {"_autoregressivehmm_dweibull_rcpp",   (DL_FUNC) &_autoregressivehmm_dweibull_rcpp,   3},
  {"_autoregressivehmm_dwrpcauchy_rcpp", (DL_FUNC) &_autoregressivehmm_dwrpcauchy_rcpp, 3},
  {"_autoregressivehmm_forward_cpp",     (DL_FUNC) &_autoregressivehmm_forward_cpp,     5},
  {NULL, NULL, 0}
};

void R_init_autoregressivehmm(DllInfo *dll)
{
  R_registerRoutines(dll, NULL, CallEntries, NULL, NULL);
  R_useDynamicSymbols(dll, FALSE);
}
