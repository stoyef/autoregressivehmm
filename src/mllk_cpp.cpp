#include "densities.h"


// Forward algorithm for likelihood computation in C++
//
// Used in mllk.R to speed up loop in forward algorithm
//
// @param allprobs Output from allprobs_cpp
// @param delta Stationary distribution
// @param Gamma Transition probability matrix
// @param autocor Matrix of autoregression coefficients
// @param nObs Number of observations
// @param nVars Number of variables
// 
// @return Negative (penalized) log-likelihood.
// [[Rcpp::export]]
double forward_cpp(arma::mat allprobs, arma::rowvec delta, arma::mat Gamma,
                   int nObs, int nVars)
  {
  
  int N = allprobs.n_cols;
  arma::rowvec foo(N);

  foo = delta % allprobs.row(0);
  double mllk_scale = log(sum(foo));
  arma::rowvec phi = foo/sum(foo);
  for (unsigned int i=1; i<allprobs.n_rows; i++){
    foo = (phi*Gamma) % allprobs.row(i);
    mllk_scale = mllk_scale + log(sum(foo));
    phi = foo/sum(foo);
  }
  return -mllk_scale;
}